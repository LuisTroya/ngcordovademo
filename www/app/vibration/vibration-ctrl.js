angular.module('ngApp')
.controller('VibrationCtrl', ['$cordovaVibration', function ($cordovaVibration) {
	var vm = this;

	vm.vibrate = function () { 
		$cordovaVibration.vibrate(500);
	}
}]);