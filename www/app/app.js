angular.module('ngApp', ['ngCordova', 'ionic'])

.run(['$ionicPlatform', function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
}])

.config(function($stateProvider, $urlRouterProvider) {

  $stateProvider
    .state('plugin', {
      url: "/plugin",
      abstract: true,
      templateUrl: "app/layout/menu-layout.html"
    })
    .state('plugin.device', {
      url: "/device",
      views: {
        'menuContent' :{
          templateUrl: "app/device/device.html",
        }
      }
    })
    .state('plugin.camera', {
      url: "/camera",
      views: {
        'menuContent' : {
          templateUrl: 'app/camera/camera.html'
        }
      }
    })
    .state('plugin.contacts', {
      url: "/contacts",
      views: {
        'menuContent' : {
          templateUrl: 'app/contacts/contacts.html'
        }
      }
    })
    .state('plugin.dialogs', {
      url: "/dialogs",
      views: {
        'menuContent' :{
          templateUrl: "app/dialogs/dialogs.html"
        }
      }
    })
    .state('plugin.network', {
      url: "/network",
      views: {
        'menuContent' :{
          templateUrl: "app/network/network.html",
        }
      }
    })
    .state('plugin.vibration', {
      url: "/vibration",
      views: {
        'menuContent' :{
          templateUrl: "app/vibration/vibration.html",
        }
      }
    })
  
  $urlRouterProvider.otherwise("/plugin/dialogs");
})
