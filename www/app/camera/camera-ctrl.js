angular.module('ngApp')
.controller('CameraCtrl', ['$cordovaCamera', '$scope', function ($cordovaCamera, $scope) {
	var vm = this;
	
	vm.takePicture = function () {
		var options = {
			quality: 50,
		    destinationType: Camera.DestinationType.DATA_URL,
		    sourceType: Camera.PictureSourceType.CAMERA,
		    allowEdit: true,
		    encodingType: Camera.EncodingType.JPEG,
		    targetWidth: 100,
		    targetHeight: 100,
		    popoverOptions: CameraPopoverOptions,
		    saveToPhotoAlbum: false
		}

		$cordovaCamera.getPicture(options).then(function (imageData) {
			// Success! Image data is here
			vm.image = "data:image/jpeg;base64," + imageData;
		}, function (err) {
			alert("An error ocurred: " + err);
		})
	}

	vm.selectPicture = function () {
		var options = {
	        destinationType: Camera.DestinationType.FILE_URI,
	        sourceType: Camera.PictureSourceType.CAMERA,
	    };

		$cordovaCamera.getPicture(options).then(function (imageUri) {
			vm.image = imageUri;
		}, function (err) {
			alert("An error ocurred: " + err);
		});
	}
}])