(function () {

	angular.module('ngApp').controller('DialogsCtrl', ['$cordovaDialogs', function ($cordovaDialogs) {
		var vm = this;

		vm.alert = function () {
			$cordovaDialogs.alert("Wow! My first ALERT!","Hello", "Close").then(function() {
				alert("Alert close!");
			});
		}

		vm.confirm = function () {
			$cordovaDialogs.confirm("Do you want to reboot your phone now?", "Reboot Phone", ["Yes", "No"]).then(function (buttonIndex) {
				// No button = 0, OK = 1, Cancel = 2
				var btnIndex = buttonIndex;
				var message = btnIndex === 1 ? "Yes" : "No";
				alert("Button selected: " + message);
			});
		}

		vm.prompt = function () {
			$cordovaDialogs.prompt("What is your name?", "Greetings", ["Say Hi", "Cancel"], "My name").then(function (result) {
				var input = result.input1;
				// No button = 0, Say Hi = 1, Cancel = 2
				var btnIndex = result.buttonIndex;
				if(btnIndex === 1) alert("Hello " + input + "!");
			});
		}
	}]);

})();