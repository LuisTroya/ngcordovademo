angular.module('ngApp')
.controller('ContactsCtrl', ['$cordovaContacts', function ($cordovaContacts) {
	var vm = this;

	vm.loadContacts = function () {
		$cordovaContacts.find({filter: ''}).then(function (results) {
			vm.contactList = filterContacts(results);
			// console.log(results);
		}, function ( error ) {
			alert("Ha ocurrido un error al listar los contactos");
		})
	}

	function filterContacts (object) {
		// Function to remove items without displayName and PhoneNumber
		return _.remove(object, function (item) {
			return item.displayName !== null && item.phoneNumbers !== null;
		});
	}

}]);