angular.module('ngApp')
.controller('NetworkCtrl', ['$cordovaNetwork', function ($cordovaNetwork) {
	var vm = this;

	vm.type = $cordovaNetwork.getNetwork();

	vm.network = function () { 
	    vm.isOnline = $cordovaNetwork.isOnline();

	    vm.isOffline = $cordovaNetwork.isOffline();
	}
}])